﻿#################### SCRIPT ATUALIZA IP E PORTA NO SECURITY GROUP AWS #####################
#                Claudio Vieira R. Junior <junior.jno@gmail.com>                          #
#                Created June, 2020                                                       #
#                Update June, 2020                                                        #

# Declaração de variáveis
$ip = (Invoke-WebRequest ifconfig.me/ip).Content #verifica IP atual da sua WAN
$bloco = "/32" #bloco de rede, pode ser /16
$SGID = "sg-08a7d60dc79101742" #SG que quer modificar
$DESCRIPTION = '`claudio`' #descrição na regra do SG,
$INGPORT = '`80`'
$PROTOCOL = 'tcp' #
$REGION = "us-east-2"
$mailOrigem = "email@email.com"
$mailDestino = "email@email.com"

#verifica na AWS no SG, ip, descrição e porta informada e retorna o IP do SG.
$ipanterior = aws ec2 describe-security-groups --group-id $SGID --output text --region $REGION --query "SecurityGroups[].IpPermissions[?FromPort == $INGPORT].[IpRanges[?Description == $DESCRIPTION].[CidrIp]]"
#Formata a variavel retirando os acentos
$DESCRIPTION = $DESCRIPTION -replace '`', ''
$INGPORT = $INGPORT -replace '`', ''

#Verifica se o IP anteorior com a descrição informada, é igual ao ip atual
if ("$ipanterior" -ne "$ip$bloco")
    {
        if ("$ipanterior" -ne "")
        { 
        aws ec2 revoke-security-group-ingress --group-id $SGID --protocol $PROTOCOL --port $INGPORT --cidr $ipanterior --region $REGION
        }

    aws ec2 authorize-security-group-ingress --group-id $SGID --region $REGION --ip-permissions IpProtocol=$PROTOCOL,FromPort=$INGPORT,ToPort=$INGPORT,IpRanges="[{CidrIp=$ip$bloco,Description='$DESCRIPTION'}]" 
    echo "Novo ip Alterado - $ip"
	aws ses send-email --from $mailOrigem --to $mailDestino --subject "Alteração do IP no $SGID" --text "Novo IP $IP$bloco alterado no Security Group $SGID com a porta $INGPORT"
    
    }

else 
{
  echo "O seu IPv4 atual e o presente no security group são os mesmos portanto não será realizado nenhuma atualização."      
}



